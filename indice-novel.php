<?php include_once 'inc/header.php'; ?>

<section class="landing">
	<div class="novel-head pt-3" data-parallax="scroll" data-image-src="img/bg/bg-1.png">
		<div class="container">
			<div class="row">
				<!-- Breadcrumb -->
				<div class="col-12">
					<nav aria-label="breadcrumb">
					  <ol class="breadcrumb">
					    <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>
					    <li class="breadcrumb-item"><a href="#">Novels</a></li>
					    <li class="breadcrumb-item active" aria-current="page">Necromante da Estação de Seul</li>
					  </ol>
					</nav>
				</div>

				<!-- Content -->
				<div class="col-md-4 text-center text-xl-left">
					<div class="novel-img">
						<img src="img/card/img-6.jpg" class="img-responsive" alt="Novel nome">
					</div>
					<div class="share">
	            		<ul class="list-inline nav">
	            			<li>
	            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fas fa-share-alt"></i></a>
	            			</li>
	            			<li>
	            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-facebook-f"></i></a>
	            			</li>
	            			<li>
	            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-twitter"></i></a>
	            			</li>
	            			<li>
	            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-facebook-messenger"></i></a>
	            			</li>
	            			<li>
	            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-pinterest-p"></i></a>
	            			</li>
	            			<li>
	            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-tumblr"></i></a>
	            			</li>
	            		</ul>
	            	</div><!-- /.share -->
				</div>

				<div class="col-md-8">
					<!-- header info -->
					<div class="novel-info">
						<!-- titulo -->
						<h1 class="font-400 mb-2 wow fadeInRight">Necromante da Estação de Seul <span class="sigla">NES</span></h1>
						<!-- info geral -->
						<ul class="list-inline nav font-14 mb-2 no-link font-300 wow fadeInRight" data-wow-delay=".2s">
							<li class="mr-3">
								<a href="#"><i class="fas fa-tags mr-1"></i> Coreana</a>
							</li>
							<li class="mr-3">
								<a href="#"><i class="fas fa-marker mr-1"></i> 2 cap. / semana</a>
							</li>
							<li class="mr-3">
								<a href="#"><i class="fas fa-book mr-1"></i> 20 Capítulos</a>
							</li>
							<li class="mr-3">
								<a href="#"><i class="fa fa-eye mr-1"></i> 50k de visualizações</a>
							</li>
						</ul>
						<!-- Autor -->
						<p class="authors font-15 font-300 wow fadeInRight" data-wow-delay=".3s">
							<b>Autor:</b> Alexandre Menin |
							<b>Tradutor:</b> Mário de Abreu |
							<b>Editor:</b> Marilia Scoot
						</p>
						<!-- evaluation -->
						<div class="evaluation lange mb-3">
			                <ul class="list-inline">
			                    <li><i class="fas fa-star"></i></li>
			                    <li><i class="fas fa-star"></i></li>
			                    <li><i class="fas fa-star"></i></li>
			                    <li><i class="far fa-star"></i></li>
			                    <li><i class="far fa-star"></i></li>
			                    <li>3.5 <small>(685 votos)</small></li>
			                </ul>
			            </div><!-- /.evaluation -->

			            <div class="w-100">
			            	<a href="single.php" class="btn btn-secundary mr-2 mb-2"><i class="fas fa-chevron-right"></i> Ler novel</a>
			            	<a href="#" class="btn btn-orange mb-2"><i class="fas fa-plus"></i> Add na biblioteca</a>
			            </div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.novel-info -->

	<div class="content-novel pb-default">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="nav">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Sobre</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="novel-cap" data-toggle="tab" href="#the-caps" role="tab" aria-controls="the-caps" aria-selected="false">Capítulos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="views-tab" data-toggle="tab" href="#views" role="tab" aria-controls="views" aria-selected="false">Avaliações</a>
							</li>
						</ul>
					</div><!-- /.nav -->

					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
							<div class="text">
								<h4>Sinopse</h4>
								<p>Quando Kang Woojin, um ex-colegial, se vê de volta à Terra após ser, involuntariamente, invocado para um planeta desconhecido por 20 anos, logo descobre que seu planeta materno não era mais o mesmo que lembrara ser um dia.</p>
								<p>Com sua antiga força e idade retornada, observe Kang Woojin conforme regressa para se tornar o Necromante mais poderoso da Terra!</p>
								<h4>Notas</h4>
								<h6>Detalhes Importantes:</h6>
								<p>"Necromante da Estação de Seul" está em processo de reestruturação, no entanto, não se preocupe em ver mudança de formatação de um capítulo específico. Por exemplo, no começo, o travessão (—) é o sinal indicativo de diálogo, mas no capítulo seguinte passa a ser apenas aspas duplas para diálogos (“”). Por agora, tenha noção que a maioria dos capítulos está em aspas, porém, o padrão listado nas dicas de leitura, logo abaixo, listarão a formatação final do projeto.</p>
								<h6>Dicas de Leitura:</h6>
								<ul class="list1">
									<li>Textos com travessão (—) representam diálogos orais de personagens.</li>
									<li>Textos em aspas duplas (“”) representam pensamentos do personagem em questão.</li>
									<li>Textos em itálico (exemplo) representam onomatopeias e conversas espirituais, mentais ou até mesmo avisos do "sistema", que não são nem pensamento e nem por voz, na obra.</li>
									<li>Por fim, não esqueça de ler as notas e colocar a leitura escura, para uma imersão maior.</li>
								</ul>
								<h4>Créditos:</h4>
								<p>Imagem de fundo foi criada pelo artista Yin Yuming.</p>
							</div><!-- /.text -->

							<div class="authors font-15 font-300">
								<h4>Informações do autor(es)</h4>
								<p><b>Autor:</b><br> Alexandre Menin</p>
								<p><b>Tradutor:</b><br> Mário de Abreu</p>
								<p><b>Editor:</b><br> Marilia Scoot</p>
							</div>

							<div class="tags">
								<h4>Tags</h4>
								<ul class="list-tags mb-0">
									<li><a href="#" title="Drama">Drama</a></li>
									<li><a href="#" title="Terror">Terror</a></li>
									<li><a href="#" title="Ficção Científica">Ficção Científica</a></li>
									<li><a href="#" title="Comédia">Comédia</a></li>
									<li><a href="#" title="Suspense">Suspense</a></li>
									<li><a href="#" title="Fantasia">Fantasia</a></li>
									<li><a href="#" title="Academia">Academia</a></li>
									<li><a href="#" title="Jogos">Jogos</a></li>
								</ul>
								<a href="#">Add minhas tags</a>
							</div><!-- /.tags -->

							<div class="suggested">
								<h4>Você também pode gostar</h4>
								<div class="novels-slider-m-col2 owl-carousel owl-theme">
									<!-- Card -->
									<?php for ($i=1; $i <= 5; $i++) { ?>
										<?php include 'inc/cards/card.php' ?>
									<?php } ?>
								</div>
							</div><!-- /.suggested -->
						</div>

						<div class="tab-pane fade" id="the-caps" role="tabpanel" aria-labelledby="novel-cap">
							<div class="alert alert-info" role="alert">
								<div class="content">
									<span>Último lançamento: <a href="single.php"> Capítulo 21: A Mommy’s Boy?</a> <small class="font-12">14 horas atrás</small></span>
								</div><!-- /.content -->
							</div><!-- /.alert -->

							<div class="capitulo">
								<?php
								//exibe os volumes
								for ($i=1; $i <= 1; $i++) { ?>
									<h3 class="vol-cap">Volume <?= $i ?></h3>
									<ol class="list-inline">
										<?php
										//exibe os arcos
										for ($i=1; $i <= 4; $i++) { ?>
											<h4 class="sub-vol">Arco <?= $i ?></h4>
											<?php
											//exibe os capitulos
											for ($k=0; $k <= 3; $k++) { ?>
												<li>
													<a href="single.php">
														<span class="vol">exemplo</span>
														<strong>Lorem ipsum dolor sit amet.</strong>
														<small>20 de setembro, 2019</small>
													</a>
												</li>
											<?php } ?>
										<?php } ?>
									</ol>
								<?php } ?>

								<?php for ($i=2; $i <= 4; $i++) { ?>
									<h3 class="vol-cap">Volume <?= $i ?></h3>
									<ol class="list-inline">
										<?php for ($j=0; $j <= 3; $j++) { ?>
											<li>
												<a href="single.php">
													<span class="vol">Volume 1</span>
													<strong>Lorem ipsum dolor sit amet.</strong>
													<small><?= $i ?> de março, 2019</small>
												</a>
											</li>
										<?php } ?>
									</ol>
								<?php } ?>
							</div>
						</div>

						<div class="tab-pane fade" id="views" role="tabpanel" aria-labelledby="views-tab">
							<div class="dark p-relative">
								<div class="evaluation lange w-450">
									<h4 class="d-inline-block mr-2">685 Avaliações</h4>
					                <ul class="list-inline d-inline-block">
					                    <li><i class="fas fa-star"></i></li>
					                    <li><i class="fas fa-star"></i></li>
					                    <li><i class="fas fa-star"></i></li>
					                    <li><i class="far fa-star"></i></li>
					                    <li><i class="far fa-star"></i></li>
					                    <li><small>3.5</small></li>
					                </ul>

									<div>
										<div class="evaluation-individual">
											<span>Qualidade de Tradução</span>
											<div class="right">
												<ul class="list-inline d-inline-block">
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><small>3.5</small></li>
								                </ul><br>
											</div>
							            </div>
										<div class="evaluation-individual">
											<span>Estabilidade de Atualizações</span>
											<div class="right">
												<ul class="list-inline d-inline-block">
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><small>1.0</small></li>
								                </ul><br>
											</div>
							            </div>
										<div class="evaluation-individual">
											<span>Desenvolvimento de História</span>
											<div class="right">
												<ul class="list-inline d-inline-block">
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><small>2.0</small></li>
								                </ul><br>
											</div>
							            </div>
										<div class="evaluation-individual">
											<span>Design de personagem</span>
											<div class="right">
												<ul class="list-inline d-inline-block">
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><small>5.0</small></li>
								                </ul><br>
											</div>
							            </div>
										<div class="evaluation-individual">
											<span>Fundo Mundial</span>
											<div class="right">
												<ul class="list-inline d-inline-block">
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="fas fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><i class="far fa-star"></i></li>
								                    <li><small>3.5</small></li>
								                </ul><br>
											</div>
							            </div>
									</div><!-- /.evaluation-individual -->

									<div class="button-evaluation">
										<p>Dê sua opnião sobre esta novel.</p>
										<button type="button" class="btn btn-orange" data-toggle="modal" data-target=".evaluation-option"><i class="far fa-comment-dots"></i> Avalie você também</button>
									</div>
					            </div><!-- /.evaluation -->
				            </div>
						</div>
					</div>
				</div>

				<!-- Ads -->
				<div class="col-12">
					<?php include 'inc/ads/anuncio.php' ?>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Avaliação modal -->
<?php include 'inc/rating/rating.php' ?>
<!-- Footer -->
<?php include_once 'inc/footer.php'; ?>
