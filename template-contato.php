<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="contact p-default s-border">
	<div class="container">
		<div class="row justify-content-center">

			<div class="col-lg-4 col-xl-3">
				<div class="border shadow p-4">
					<h4>Informações para contato</h4>
					<ul class="list-inline mb-0">
						<li class="mb-2">
							<i class="fa fa-envelope text-orange"></i> contato@exemplo.com
						</li>
						<li>
							<i class="fa fa-phone text-orange"></i> (22) 99999-9999
						</li>
					</ul>
				</div>

				<ul class="social list-inline mt-3 text-center">
					<li><a href="https://facebook.com/novelmania" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-facebook-f"></i></a></li>
					<li><a href="https://twitter.com/novelmaniabr" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-twitter"></i></a></li>
					<li><a href="https://www.instagram.com/novelmaniaoficial/" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-instagram"></i></a></li>
					<li><a href="https://youtube.com/c/NovelMania" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-youtube"></i></a></li>
				</ul>
			</div>

			<div class="col-lg-5 col-xl-6">
				<div class="border shadow p-3 p-sm-4">
					<h2 class="mb-0 wow fadeInLeft">Fale conosco</h2>
					<p class="text-grey">Entre em contato conosco respondendo o formulário abaixo, ou por e-mail.</p>
					<form action="#" autocomplete="off">
						<div class="mb-2">
							<label for="form_name">Nome <sup>*</sup></label>
							<input type="text" id="form_name" class="form-control" required="">
						</div>

						<div class="mb-2">
							<label for="form_email">Seu e-mail <sup>*</sup></label>
							<input type="email" id="form_email" class="form-control" required="">
						</div>

						<div class="mb-2">
							<label for="form_subject">Assunto <sup>*</sup></label>
							<select id="form_subject" class="form-control" required="">
								<option value="" selected disabled>Escolher opção</option>
								<option value="">Recrutamento</option>
								<option value="">Parceria</option>
								<option value="">Pedidos</option>
								<option value="">Dúvida</option>
								<option value="">Sugestão</option>
								<option value="">Reclamação</option>
								<option value="">Erro</option>
							</select>
							<i class="fa fa-chevron-down"></i>
						</div>

						<div class="mb-2">
							<label for="form_messege">Mensagem <sup>*</sup></label>
							<textarea type="text" id="form_messege" class="form-control" required=""></textarea>
						</div>

						<button type="submit" class="btn btn-primary">Enviar</button>
					</form>
				</div>
			</div>

			<div class="col-lg-3 mt-3 mt-lg-0">
				<?php include 'inc/ads/anuncio2.php' ?>
			</div><!-- End anúncio -->

		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- End contact -->


<?php include_once 'inc/footer.php'; ?>
