<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="landing p-default s-border">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-lg-3 text-center text-md-left">
				<div id="politica-privacidade" class="list-group wow fadeInUp">
				    <a class="list-group-item list-group-item-action" href="#list-item-1" rel="data-smooth">Nossa Política</a>
				    <a class="list-group-item list-group-item-action" href="#list-item-2" rel="data-smooth">Os anúncios</a>
				    <a class="list-group-item list-group-item-action" href="#list-item-3" rel="data-smooth">Cookie DoubleClick Dart</a>
				    <a class="list-group-item list-group-item-action" href="#list-item-4" rel="data-smooth">Os Cookies e Web Beacons</a>
				    <a class="list-group-item list-group-item-action" href="#list-item-5" rel="data-smooth">Ligações a Sites de terceiros</a>
				</div>

				<div class="my-3 wow fadeInUp">
					<?php include 'inc/ads/anuncio2.php' ?>
				</div>
			</div><!-- /.col-md-3 -->

			<div class="col-md-7 col-lg-9">
				<article class="content">
					<h1 id="list-item-1" class="wow fadeInUp">Política de privacidade para Novel Mania</h1>
					<p>Todas as suas informações pessoais recolhidas, serão usadas para o ajudar a tornar a sua visita no nosso site o mais produtiva e agradável possível.</p>
					<p>A garantia da confidencialidade dos dados pessoais dos utilizadores do nosso site é importante para a Novel Mania.</p>
					<p>Todas as informações pessoais relativas a membros, assinantes, clientes ou visitantes que usem a Novel Mania serão tratadas em concordância com a Lei da Proteção de Dados Pessoais de 26 de outubro de 1998 (Lei n.º 67/98).</p>
					<p>A informação pessoal recolhida pode incluir o seu nome, e-mail, número de telefone e/ou telemóvel, morada, data de nascimento e/ou outros.</p>
					<p>O uso da Novel Mania pressupõe a aceitação deste Acordo de privacidade. A equipa da Novel Mania reserva-se ao direito de alterar este acordo sem aviso prévio. Deste modo, recomendamos que consulte a nossa política de privacidade com regularidade de forma a estar sempre atualizado.</p>

					<h2 id="list-item-2" class="wow fadeInUp">Os anúncios</h2>
					<p>Tal como outros websites, coletamos e utilizamos informação contida nos anúncios. A informação contida nos anúncios, inclui o seu endereço IP (Internet Protocol), o seu ISP (Internet Service Provider, como o Sapo, Clix, ou outro), o browser que utilizou ao visitar o nosso website (como o Internet Explorer ou o Firefox), o tempo da sua visita e que páginas visitou dentro do nosso website.</p>

					<h2 id="list-item-3" class="wow fadeInUp">Cookie DoubleClick Dart</h2>
					<p>O Google, como fornecedor de terceiros, utiliza cookies para exibir anúncios no nosso website;</p>
					<p>Com o cookie DART, o Google pode exibir anúncios com base nas visitas que o leitor fez a outros websites na Internet;</p>
					<p>Os utilizadores podem desativar o cookie DART visitando a Política de <a title="privacidade da rede de conteúdo" href="http://politicaprivacidade.com/">privacidade da rede de conteúdo</a> e dos anúncios do Google.</p>

					<h2 id="list-item-4" class="wow fadeInUp">Os Cookies e Web Beacons</h2>
					<p>Utilizamos cookies para armazenar informação, tais como as suas preferências pessoas quando visita o nosso website. Isto poderá incluir um simples popup, ou uma ligação em vários serviços que providenciamos, tais como fóruns.</p>
					<p>Em adição também utilizamos publicidade de terceiros no nosso website para suportar os custos de manutenção. Alguns destes publicitários, poderão utilizar tecnologias como os cookies e/ou web beacons quando publicitam no nosso website, o que fará com que esses publicitários (como o Google através do Google AdSense) também recebam a sua informação pessoal, como o endereço IP, o seu ISP, o seu browser, etc. Esta função é geralmente utilizada para geotargeting (mostrar publicidade de Lisboa apenas aos leitores oriundos de Lisboa por ex.) ou apresentar publicidade direcionada a um tipo de utilizador (como mostrar publicidade de restaurante a um utilizador que visita sites de culinária regularmente, por ex.).</p>
					<p>Você detém o poder de desligar os seus cookies, nas opções do seu browser, ou efetuando alterações nas ferramentas de programas Anti-Virus, como o Norton Internet Security. No entanto, isso poderá alterar a forma como interage com o nosso website, ou outros websites. Isso poderá afetar ou não permitir que faça logins em programas, sites ou fóruns da nossa e de outras redes.</p>

					<h2 id="list-item-5" class="wow fadeInUp">Ligações a Sites de terceiros</h2>
					<p>A Novel Mania possui ligações para outros sites, os quais, a nosso ver, podem conter informações/ferramentas úteis para os nossos visitantes. A nossa política de privacidade não é aplicada a sites de terceiros, pelo que, caso visite outro site a partir do nosso deverá ler a politica de privacidade do mesmo.</p>
					<p>Não nos responsabilizamos pela política de privacidade ou conteúdo presente nesses mesmos sites.</p>
				</article>
			</div>
		</div>
	</div>
</section>

<?php include_once 'inc/footer.php'; ?>
