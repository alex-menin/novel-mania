<?php include_once 'inc/header.php'; ?>
<link rel="stylesheet" href="css/sidebar.css">

<section class="landing novel-single">
	<div class="novel-head pt-3" data-parallax="scroll" data-image-src="img/bg/bg-1.png" class="img-responsive">
		<div class="container">
			<div class="row justify-content-center">
				<!-- Breadcrumb -->
				<div class="col-md-12">
					<nav aria-label="breadcrumb">
					    <ol class="breadcrumb">
					        <li class="breadcrumb-item"><a href="./"><i class="fa fa-home"></i> Início</a></li>
					        <li class="breadcrumb-item"><a href="novels.php">Novels</a></li>
					        <li class="breadcrumb-item"><a href="indice-novel.php">Necromante da Estação de Seul</a></li>
					        <li class="breadcrumb-item active" aria-current="page">Atual</li>
					    </ol>
					</nav>
				</div>

				<!-- Content -->
				<div class="col-md-12">
					<!-- header info -->
					<div class="novel-info">
						<!-- titulo -->
						<h1 class="font-400 mb-2 wow fadeInRight">
							<a href="indice-novel.php" class="text-white">Necromante da Estação de Seul</a> <span class="sigla">NES</span>
						</h1>

						<h3 class="subtitle wow fadeInRight" data-wow-delay=".2s">Capítulo 1: Até depois dos coráis</h3>
						<p class="wow fadeInRight" data-wow-delay=".3s"><b>Autor: </b> Jinseol-u</p>

						<div class="share">
		            		<ul class="list-inline nav">
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fas fa-share-alt"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-facebook-f"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-twitter"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-facebook-messenger"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-pinterest-p"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-tumblr"></i></a>
		            			</li>
		            		</ul>
		            	</div><!-- /.share -->
					</div>
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.novel-info -->

	<div class="content-novel p-default dark">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-md-10 col-lg-8">
					<div class="text">
						<h2>1</h2>
						<p>Cumque odio, nisi rem culpa at maiores? Suscipit aliquid, quibusdam praesentium odit ullam officiis, officia quae! Dicta eius voluptas, optio magni non ex sunt ad molestiae unde impedit repellat odio doloremque eaque sit perspiciatis expedita similique consequuntur porro aut. Non nisi amet repudiandae laborum inventore nulla numquam odio tempore eveniet similique quos illum blanditiis, quam totam et ratione facere soluta debitis necessitatibus incidunt culpa sunt magni dolorum animi, quaerat! Aut molestias vero nisi neque odio consequatur iure unde sed dignissimos doloribus quaerat aliquid officia vel saepe autem earum, modi, aperiam, repellendus quisquam ullam consequuntur ab iusto hic. Dolores non laboriosam accusamus explicabo quam sed provident fuga et unde suscipit numquam, tenetur velit qui iusto ratione adipisci esse dolorum ullam doloribus!</p>
					</div><!-- /.text -->

					<div class="credits">
						<p>
							<b>Tradutor:</b> Nome da pessoa
						</p>
					</div>

					<div class="pagination justify-content-between">
						<div class="p-prev">
							<a href="" title="Cápítulo anterior"><i class="fas fa-chevron-left"></i> Anterior</a>
						</div>
						<div class="indice">
							<a href="indice-novel.php" title="Ver índice">Índice</a>
						</div>
						<div class="p-next">
							<a href="" title="Próximo capítulo">Próximo <i class="fas fa-chevron-right"></i></a>
						</div>
					</div>

					<hr>

		           	<div class="comments">
		           		<h3 class="mb-0"><i class="far fa-comments"></i> Comentários</h3>
		           		<div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="5"></div>
		           	</div><!-- /.comments -->

		           	<!-- Ads -->
					<div class="col-12 mt-3">
						<?php include 'inc/ads/anuncio.php' ?>
					</div>


				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.content-novel -->
</section>


<?php include 'inc/sidebar.php' ?>
<?php include_once 'inc/footer.php'; ?>
