##Plugins
en-US - This theme includes:
pt-BR - Este tema inclue:

  - [Modernizr](https://modernizr.com/) - v3.5.0
  - [jQuery](https://jquery.com/) - v3.3.1
  - [Popper](https://popper.js.org/) - v1.14.4
  - [Bootstrap](https://getbootstrap.com/) - v4.1.3
  - [Owl Carousel](https://owlcarousel2.github.io/OwlCarousel2/) - v2.3.4
  - [Font Awesome Free](https://fontawesome.com) - v5.6.3
  - [LightGallery](http://sachinchoolur.github.io/lightGallery/) - v1.6.11
  - [jQuery Mask Plugin](https://igorescobar.github.io/jQuery-Mask-Plugin/) - v1.14.15
  - [WOW Animate Plugin](https://wowjs.uk/) - v1.3.0
  - [iziToast](http://izitoast.marcelodolce.com/) - v1.4.0
