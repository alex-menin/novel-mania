<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="novels p-default s-border">
	<div class="container">
		<div class="row">

			<div class="col-12 mb-4">
				<div class="row">
					<div class="col-12 text-center text-md-left">
						<span class=" mr-3 d-none d-md-inline-block">Filtros</span>

						<div class="button" title="Buscar novels relacionadas">
							<i class="fas fa-search-plus d-inline-block"></i>
						</div>

						<div class="button" title="Minhas novels">
							<i class="far fa-heart d-inline-block"></i>
						</div>

						<div class="button" title="Favoritas">
							<i class="far fa-bookmark d-inline-block"></i>
						</div>

						<div class="button" title="Lidas">
							<i class="fas fa-book d-inline-block"></i>
						</div>

						<div class="button" title="Leituras incompletas">
							<i class="fas fa-book-open d-inline-block"></i>
						</div>

						<div class="button" title="Remover todas">
							<i class="far fa-trash-alt d-inline-block"></i>
						</div>

					</div>
				</div>
			</div>

			<?php for ($i=0; $i < 8; $i++) { ?>
			<div class="top-novels library-list dark col-lg-6 pb-3">
				<div class="box">
					<span class="favorite"><i class="far fa-bookmark"></i> Favorita
						<a href="javascript:void(0);" class="remove"><i class="fas fa-minus-circle"></i></a>
					</span>

					<h5 class="mb-1 mt-sm-2 mt-lg-0">Lorem ipsum dolor sit.Lorem ipsum dolor sit amet, consectetur.</h5>
					<div class="author">Autores: Mariana Clara, Luiz de Assis</div>

					<ul>
						<li><small><i class="fab fa-readme"></i> 25% lida</small></li>
					</ul>

			        <div>
			        	<a href="indice-novel.php" class="btn btn-orange btn-small"><i class="fas fa-chevron-right mr-1"></i> Ler novel</a>
			        	<a href="#" class="btn btn-orange btn-small"><i class="fas fa-minus-circle"></i> Remover novel</a>
			        </div>
				</div>

			</div><!-- /.top-novels -->
			<?php } ?>


		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.novels -->


<?php include_once 'inc/footer.php'; ?>
