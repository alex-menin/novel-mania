<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="landing p-default s-border">
	<div class="container">
		<article class="content">
			<div class="row">
				<div class="col-md-12">
					<!-- Title -->
					<div class="text-center">
						<img src="img/content/contribuindo-com-a-seita.png" alt="Contribuindo com a Seita" class="img-responsive">
						<p class="subtitle">A partir deste Salão você pode contribuir com o desenvolvimento de nossa Seita e garantir que ela irá continuar subindo aos céus!</p>
					</div>

					<h2 class="mt-4">Contribuindo com a Seita</h2>
					<p>Não há sentimento melhor do que ver os apoios financeiros crescendo com a ajuda de amigos, familiares e fãs. Caso você tenha o desejo de que nossa Seita continue em crescimento você pode humildemente realizar uma doação.</p>
					<p>Precisamos da força de todos, uma união para que possamos continuar, para que possamos crescer! Você pode doar qualquer valor, ou pode doar seu tempo se tornando um Élder na seita.</p>
				</div>
			</div>

			<!-- Pagamentos -->
			<div class="row">
				<div class="col-md-12 text-center mb-4 ">
					<hr>
					<img src="img/content/quero-apoiar.png" alt="Quero Apoiar" class="mt-4" class="img-responsive">
					<p class="subtitle">Clique em um dos botões abaixo e faça sua contribuição!</p>
				</div>

				<div class="col-sm-6 mb-4 text-center">
					<a href="javascript:void(0);" target="_blank">
						<div class="border shadow paypal">
							<img src="img/content/paypal.png" alt="PayPal">
						</div>
					</a>
				</div>

				<div class="col-sm-6 mb-4 text-center">
					<a href="javascript:void(0);" target="_blank">
						<div class="border shadow pagseguro">
							<img src="img/content/pagseguro.png" alt="PagSeguro">
						</div>
					</a>
				</div>

				<div class="col-md-12 text-center">
					<div class="mb-5">
						<p class="subtitle mb-0">Após realizar sua doação <a href="https://docs.google.com/forms/d/e/1FAIpQLSdIuNxQNX1_RWjh_zGK3HRAxMKzEANUlZGzwcMoibIsJdx2cg/viewform" target="_blank"><b>Preencha este Formulário</b></a>, para identificarmos sua contribuição.</p>
					</div>

					<div class="mb-5">
						<h3><i class="fas fa-hand-holding-usd"></i> Apoio à Seita Novel Mania</h3>
						<div class="progress">
					  		<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
						</div>
						<p> Meta mensal » <b>R$ 300,00</b></p>
						<small>O apoio financeiro será convertido em anúncios para atrair mais discípulos, pagamento mensal do servidor, domínio anual, melhorias internas e manutenções.</small>
					</div>
					<hr>
				</div>
			</div>
			<!-- End pagamentos -->

			<div class="row">
				<div class="col-md-6">
					<h2>Profundo texto patrocinado</h2>
					<p>O Profundo Texto Patrocinado visa aumentar o número de profundos textos decifrados. Esta é basicamente uma opção para incentivar nossos Élderes a lançarem mais capítulos acima da agenda regular, sem prejudicar os lançamentos padrões!</p>
					<p>» Para mais informações visite Profundo Texto Patrocinado «</p>
				</div>

				<div class="col-md-6">
					<h2>Desafios</h2>
					<p>A NovelMania não possui desafios no momento.</p>
				</div>
			</div>
		</article>
	</div>
</section>


<?php include_once 'inc/footer.php'; ?>
