<?php include_once 'inc/header.php'; ?>

<section class="profile-top">
	<div class="feature-photo">

		<!-- Cover picture -->
		<figure class="bg-cover" style="background:url('img/profile/timeline-1.jpg');"></figure>

		<!-- Edit cover picture -->
		<form class="edit-phto cover">
			<i class="fa fa-camera-retro"></i>
			<label class="fileContainer">
				<span>Editar</span>
				<input type="file"/>
			</label>
		</form>

		<div class="container-fluid">
			<div class="row merged">
				<!-- Avatar picture -->
				<div class="col-sm-4 col-md-3 flex-center">
					<div class="user-avatar">
						<figure>
							<img src="https://instagram.fcaw1-1.fna.fbcdn.net/vp/2f393c01c14faef6026309f679492820/5DA52751/t51.2885-15/sh0.08/e35/p750x750/65034981_865165327181361_5412276586981504749_n.jpg?_nc_ht=instagram.fcaw1-1.fna.fbcdn.net" alt="">
							<form class="edit-phto img-profile">
								<i class="fa fa-camera-retro"></i>
								<label class="fileContainer">
									Editar foto
									<input type="file"/>
								</label>
							</form>
						</figure>
					</div>
				</div>

				<!-- Links -->
				<div class="col-sm-8 col-md-9 d-flex align-items-center">
					<div class="title-info">
						<ul>
							<li class="admin-name">
							    <h5>Alexandre Menin <span class="level-p">
								    <span>Level 10</span>
							    </span></h5>
							</li>
							<li class="id-user">ID: 28464853</li>
							<li class="btn-follow"><a href="javascript:void(0);" class="btn">Seguir</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
</section><!-- top area -->

<!-- Content -->
<section class="content-timeline">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-lg-3 text-center text-md-left">
				<div class="box">
					<ul class="info-member list-inline">
						<li><i class="far fa-calendar-alt"></i> Entrou em 12 Set, 2019</li>
						<li><i class="fas fa-user-friends"></i> 6.254.450 seguidores</li>
						<li><i class="fas fa-book mr-2"></i> <a href="biblioteca.php">Minha biblioteca</a></li>
						<li><a href="template-editar.php"><i class="far fa-edit"></i> Editar dados</a></li>
					</ul>
				</div>
			</div>

			<div class="col-md-8 col-lg-9 mt-3 mt-md-0 text-center text-md-left">
				<h3>Minhas novels</h3>

				<div class="novels row">
					<?php for ($i=0; $i < 8; $i++) { ?>
						<div class="top-novels library-list dark col-lg-6 pb-3">
							<div class="box">
								<span class="favorite"><i class="far fa-bookmark"></i> Favorita
									<a href="javascript:void(0);" class="remove"><i class="fas fa-minus-circle"></i></a>
								</span>
								<h5 class="mb-1 mt-sm-2 mt-lg-0">Lorem ipsum dolor sit.Lorem ipsum dolor sit amet, consectetur.</h5>
								<div class="author">Autores: Mariana Clara, Luiz de Assis</div>
						        <div class="mt-2">
						        	<a href="indice-novel.php" class="btn btn-orange btn-small"><i class="fas fa-chevron-right mr-1"></i> Ler novel</a>
						        	<a href="#" class="btn btn-orange btn-small"><i class="far fa-edit"></i> Editar novel</a>
						        </div>
							</div>
						</div><!-- /.top-novels -->
						<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>


<?php include_once 'inc/footer.php'; ?>
