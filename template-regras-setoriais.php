<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="landing p-default s-border">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<article class="content">
					<h1>Estes são os termos e regras de nossa Seita!</h1>
					<h3>1. Termos</h3>
					<p>Acessando nossa Biblioteca/Site, você concorda estar sujeito por esses Termos e Condições de Uso, todas as leis aplicáveis e regulamentos e você concorda que é responsável pela conformidade com quaisquer leis locais aplicáveis. Se você não concorda com nenhum desses termos, você é proibido de usar ou acessar nossa Biblioteca. Os materiais contidos aqui são protegidos por Copyright aplicáveis e leis de marca registrada.</p>

					<h3>2. Uso dos Profundos Textos</h3>
					<p>1.  A Permissão é garantida para baixar temporariamente uma cópia dos materiais em Novel Mania(™) para visualização transitória pessoal. Essa é a concessão de uma permissão, não uma transferência de título e sob essa permissão você não pode:</p>
					<ol>
						<li>Modificar ou copiar os materiais;</li>
						<li>Usar os materiais para quaisquer propósitos comerciais ou por exibição pública (comercial ou não-comercial);</li>
						<li>Tentativa de descompilar ou fazer engenharia reversa de qualquer software contido no website da Novel Mania;</li>
						<li>Remover quaisquer direitos autorais ou outras anotações de propriedade dos materiais;</li>
						<li>Transferir os materiais para outra pessoa ou “hospedar” os materiais em qualquer outro servidor.</li>
					</ol>
					<p>2. Essa licença cessará automaticamente se você violar quaisquer dessas restrições e pode ser rescindido pela Novel Mania a qualquer hora. Ao terminar a visualização desses materiais ou ao término dessa licença, você deve destruir quaisquer materiais baixados em sua posse, seja em formato eletrônico ou impresso, impedindo assim de que os mesmos vazem para o mundo exterior.</p>
				</article>
			</div>

			<div class="col-lg-5">
				<img src="img/content/heroi.png" alt="" class="img-responsive">
			</div>

			<div class="col-lg-12 mt-3">
				<?php include 'inc/ads/anuncio.php' ?>
			</div>
		</div>
	</div>
</section>


<?php include_once 'inc/footer.php'; ?>
