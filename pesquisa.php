<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="search p-default s-border">
	<div class="container">
		<div class="row justify-content-center">

			<div class="col-lg-12 mb-3">
				<?php include 'inc/ads/anuncio.php' ?>
			</div><!-- End anúncio -->

			<div class="col-lg-12">
				<h2 class="mb-0 wow fadeInLeft">Resultados da pesquisa</h2>
				<p class="text-grey">5 resultados encontrados com o termo "Novel".</p>
				<form action="pesquisa.php" method="post" class="search-form">
				    <div class="input-group mb-3">
				        <input type="search" class="form-control" name="search" placeholder="Procurar novel...">
				        <div class="input-group-prepend">
							<button type="submit"><i class="fa fa-search"></i></button>
				        </div>
				    </div>
				</form>
			</div>

			<div class="col-lg-12">
				<div class="row">
					<!-- Card -->
					<?php for ($i=1; $i < 6; $i++) { ?>
						<div class="col-sm-6 col-md-4 col-lg-3">
							<?php include 'inc/cards/card.php' ?>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="col-12">
				<nav aria-label="Pagination">
					<ul class="pagination justify-content-center mb-0">
						<li class="page-item">
							<span class="page-link"><i class="fa fa-angle-double-left"></i></span>
						</li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item active" aria-current="page">
							<span class="page-link">
								2
								<span class="sr-only">(current)</span>
							</span>
						</li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item">
							<a class="page-link" href="#"><i class="fa fa-angle-double-right"></i></a>
						</li>
					</ul>
				</nav>
			</div>

		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.search -->


<?php include_once 'inc/footer.php'; ?>
