<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="novels p-default s-border">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<div class="row">

					<?php for ($i=1; $i < 12; $i++) {
						include 'inc/cards/card-2.php';
					} ?>

				</div><!-- /.row -->
			</div><!-- /.col-12 -->

			<div class="col-12">
				<nav aria-label="Pagination">
					<ul class="pagination justify-content-center mb-0">
						<li class="page-item">
							<span class="page-link"><i class="fa fa-angle-double-left"></i></span>
						</li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item active" aria-current="page">
							<span class="page-link">
								2
								<span class="sr-only">(current)</span>
							</span>
						</li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item">
							<a class="page-link" href="#"><i class="fa fa-angle-double-right"></i></a>
						</li>
					</ul>
				</nav>
			</div>

		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.novels -->


<?php include_once 'inc/footer.php'; ?>
