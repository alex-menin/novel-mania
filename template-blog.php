<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="blog list p-default s-border">
	<div class="container">
		<div class="row justify-content-center">

			<div class="col-lg-8">

				<?php for ($i=0; $i < 5; $i++) { ?>
				<div class="item-post new mb-2 mb-md-3">
					<a href="single-blog.php">
						<div class="row no-gutters">
							<div class="col-4 col-sm-3 col-md-5 item-header">
								<div class="over">
									<img src="http://via.placeholder.com/400x200" alt="" class="img-responsive">
								</div>
							</div>
							<div class="col-8 col-sm-9 col-md-7 p-2 p-md-3 d-flex align-items-center">
								<div>
									<h2 class="title">
										Título da postagem novel Título da postagem novel mania
									</h2>
									<span class="info-down">
										<i class="fa fa-clock"></i> 25 de junho de 2019
									</span>
									<span class="info-down ml-sm-2">
										<i class="fas fa-user"></i> Henrique Admin
									</span>
								</div>
							</div>
						</div>
					</a>
				</div><!-- /.item-post -->
				<?php } ?>

			<nav aria-label="Pagination">
				<ul class="pagination justify-content-center mb-0 mt-4 mt-lg-0">
					<li class="page-item">
						<span class="page-link"><i class="fa fa-angle-double-left"></i></span>
					</li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item active" aria-current="page">
						<span class="page-link">
							2
							<span class="sr-only">(current)</span>
						</span>
					</li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item">
						<a class="page-link" href="#"><i class="fa fa-angle-double-right"></i></a>
					</li>
				</ul>
			</nav>

			</div><!-- /.col-md-9 -->

			<div class="col-lg-4">
				<div class="widget mt-lg-0">
					<h3>Últimas atualizações</h3>
					<?php for ($i=0; $i < 3; $i++) { ?>
					<div class="item-post mb-2">
						<a href="indice-novel.php">
							<div class="row no-gutters">
								<div class="col-4 col-sm-3 col-md-2 col-lg-3 item-header">
									<img src="img/card/img-6.jpg" alt="" class="img-responsive">
								</div>
								<div class="col-8 col-sm-9 col-md-10 col-lg-9 p-2 p-md-3 p-lg-2">
									<h2 class="title">
										Título da postagem novel Título da postagem novel mania
									</h2>
									<span class="info-down"><i class="fa fa-clock"></i> 25 de junho de 2019</span>
								</div>
							</div>
						</a>
					</div>
					<?php } ?>
				</div><!-- /.widget -->

				<div class="widget">
					<h3>Tags sugeridas</h3>
					<ul class="list-tags">
						<li><a href="#" title="Drama">Drama</a></li>
						<li><a href="#" title="Terror">Terror</a></li>
						<li><a href="#" title="Ficção Científica">Ficção Científica</a></li>
						<li><a href="#" title="Comédia">Comédia</a></li>
						<li><a href="#" title="Suspense">Suspense</a></li>
						<li><a href="#" title="Fantasia">Fantasia</a></li>
						<li><a href="#" title="Academia">Academia</a></li>
						<li><a href="#" title="Jogos">Jogos</a></li>
					</ul>
				</div><!-- /.widget -->

				<div class="widget">
					<?php include 'inc/ads/anuncio3.php' ?>
				</div><!-- /.widget -->
			</div>

		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- End blog -->


<?php include_once 'inc/footer.php'; ?>
