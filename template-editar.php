<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="landing p-default s-border">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-6">
				<div class="box">
					<h4 class="text-center">Editar dados pessoais</h4>

					<form  action="javascript:void(0);">
						<!-- imagem -->
						<div class="text-center text-sm-left d-sm-flex align-items-center justify-content-center">
							<div class="user-avatar mini">
								<figure>
									<img src="https://instagram.fcaw1-1.fna.fbcdn.net/vp/2f393c01c14faef6026309f679492820/5DA52751/t51.2885-15/sh0.08/e35/p750x750/65034981_865165327181361_5412276586981504749_n.jpg?_nc_ht=instagram.fcaw1-1.fna.fbcdn.net" alt="">
									<div class="edit-phto img-profile">
										<i class="fa fa-camera-retro"></i>
										<label class="fileContainer">
											Editar foto
											<input type="file"/>
										</label>
									</div>
								</figure>
							</div>
							<label class="ml-3">Editar imagem de perfil</label>
						</div>

						<!-- inputs -->
						<div class="mb-2">
							<label for="">Nome</label>
							<input type="text" value="Alexandre Menin" class="form-control">
						</div>

						<div class="mb-2">
							<label for="">E-mail</label>
							<input type="text" value="exemplo@gmail.com" class="form-control">
						</div>

						<div class="mb-2">
							<label for="">Senha</label>
							<input type="text" class="form-control" placeholder="Nova senha">
						</div>

						<div class="mt-3 text-center">
							<button class="btn btn-primary btn-small" type="submit">Salvar alterações</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>


<?php include_once 'inc/footer.php'; ?>
