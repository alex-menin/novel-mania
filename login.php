<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#e0e0e0">
	<meta name="description" content="Mergulhe na cultura oriental. Leia Light Novels &amp; WebNovels Chinesas, Japonesas e Coreanas em Português.">

	<title>Início - Novel Mania</title>

	<link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico'/>
	<link rel="stylesheet" href="css/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="fontawesome/css/all.min.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/main.css">

</head>

	<section class="login">
		<div class="row no-gutters">

			<div class="login-left col-md-6 order-1 order-md-0">
				<h1 class="wow fadeInUp">Entre agora <br>em sua <span>conta</span></h1>
			</div>

			<div class="login-right col-md-6 order-0 order-md-1">
				<form action="">
					<div class="logo">
						<a href="./">
							<img src="img/logo.png" alt="Novel Mania">
						</a>
					</div>

					<div class="form-group mb-2">
						<label for="form_email">E-mail</label>
						<input type="email" class="form-control" id="form_email" placeholder="Coloque seu e-mail">
					</div>

					<div class="form-group mb-3">
						<label for="form_password">Senha</label>
						<input type="password" class="form-control" id="form_password" placeholder="Coloque sua senha">
					</div>

					<button class="btn btn-primary">Enviar</button>
				</form>

				<div class="forgot">
					<span>
						<a href="#">Esqueceu sua senha?</a>
					</span>

					<span>
						<a href="#">Criar conta</a>
					</span>
				</div>
			</div>

		</div>
	</section><!-- End login -->


	<script type="text/javascript" src="js/vendor/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/wow.min.js"></script>
	<script type="text/javascript" src="plugins/parallax/parallax.js"></script>
	<script type="text/javascript" src="js/novel.js"></script><!-- Single.php -->
	<script type="text/javascript" src="js/main.js"></script>


</body>
</html>
