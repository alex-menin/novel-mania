<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<!-- 404 -->
<div class="error-404 s-border">
	<div class="container">
		<div class="row">

			<div class="col-12 text-center">
				<img src="img/error-404.png" alt="404 page" class="wow fadeInDown">
				<h1 class="mt-4 wow fadeInUp">404</h1>
				<h4 class="mb-2 wow fadeInUp">Página não encontrada =(</h4>
				<p class="wow fadeInUp" data-wow-delay=".25s">Oops, desculpe mas a página que você acessou não existe.</p>
				<a class="btn btn-primary wow fadeInUp" data-wow-delay=".3s" href="./">Voltar ao início</a>
			</div>

			<div class="col-12 mt-4">
				<?php include 'inc/ads/anuncio.php' ?>
			</div>

		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- End error-404  -->


<?php include_once 'inc/footer.php'; ?>
