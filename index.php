<?php include_once 'inc/header.php'; ?>


<!-- Slider welcome -->
<section class="welcome border-w">
	<div class="slider owl-carousel owl-theme">
		<!-- Tamanhos usandos | 1920x650 | 1200x650 | 768x500 -->
		<div class="item">
			<img src="img/bg/bg-banner.jpg" alt="Novel Mania" class="d-none d-lg-block">
			<img src="img/bg/bg-banner-2.jpg" alt="Novel Mania" class="d-none d-md-block d-lg-none">
			<img src="img/bg/bg-banner-3.jpg" alt="Novel Mania" class="d-block d-md-none">
		</div>

		<div class="item">
			<img src="img/bg/bg-banner-novel.jpg" alt="Novel Mania" class="d-none d-lg-block">
			<img src="img/bg/bg-banner-novel-2.jpg" alt="Novel Mania" class="d-none d-md-block d-lg-none">
			<img src="img/bg/bg-banner-novel-3.jpg" alt="Novel Mania" class="d-block d-md-none">
		</div>

	</div>
</section><!-- /end slider welcome -->


<!-- Blog -->
<section class="blog pt-default">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="title wow flipInX">Últimas novidades</h1>
			</div>

			<div class="col-md-6">
				<div class="item-post new mb-2">
					<a href="single-blog.php">
						<div class="row no-gutters">
							<div class="col-4 col-sm-3 col-md-12 item-header">
								<div class="over">
									<img src="http://via.placeholder.com/800x300" alt="" class="img-responsive">
								</div>
							</div>
							<div class="col-8 col-sm-9 col-md-12 p-2 p-sm-3">
								<h2 class="title h6">
									Título da postagem novel Título da postagem novel mania
								</h2>
								<span class="info-down">
									<i class="fa fa-clock"></i> 25 de junho de 2019
								</span>
								<span class="info-down ml-sm-2">
									<i class="fas fa-user"></i> Henrique Admin
								</span>
							</div>
						</div>
					</a>
				</div><!-- /.item-post -->
			</div>

			<div class="col-md-6">
				<?php include 'inc/cards/card-blog.php' ?>
				<?php include 'inc/cards/card-blog.php' ?>
				<?php include 'inc/cards/card-blog.php' ?>
			</div>

		</div>
	</div>
</section>


<!-- Recem chegadas -->
<section class="pt-default">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h1 class="title wow flipInX">Recém chegadas</h1>
			</div>

			<div class="col-12">
				<div class="novels-slider-m-col2 owl-carousel owl-theme">
					<!-- Card -->
					<?php for ($i=1; $i < 6; $i++) { ?>
						<?php include 'inc/cards/card.php' ?>
					<?php } ?>
				</div>
			</div>

			<!-- Ads -->
			<div class="col-12">
				<?php include 'inc/ads/anuncio.php' ?>
			</div>
		</div>
	</div>
</section>


<!-- Populares -->
<section class="pt-default">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h1 class="title wow flipInX">Populares</h1>
			</div>

			<div class="col-12">
				<div class="novels-slider-col5 owl-carousel owl-theme">
					<!-- Card -->
					<?php for ($i=1; $i < 6; $i++) { ?>
						<?php include 'inc/cards/card.php' ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Alerts -->
<section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="alert alert-success" role="alert">
					<a href="javascript:void(0);">
						<div class="content">
							<h3 class="d-inline-block mb-0">Qual a sua favorita?</h3>
							<p class="d-inline-block mb-0">Classifique as melhores novels agora</p>
						</div><!-- /.content -->
					</a>
				</div><!-- /.alert -->
			</div>
		</div>
	</div>
</section>


<!-- novels -->
<section class="novels pt-mb-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="title wow flipInX">Mais Lidas</h1>
			</div>
			<?php for ($i=1; $i <= 4; $i++) { ?>
				<?php include 'inc/cards/card-3.php' ?>
			<?php } ?>

			<div class="col-12 text-center wow fadeInUp">
				<a href="novels.php" class="btn btn-secundary">Veja Mais</a>
			</div>
		</div>
	</div>
</section>


<!-- Recomendações -->
<section class="p-default">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h1 class="title wow flipInX">Recomendadas para ler</h1>
			</div>

			<div class="col-12">
				<div class="novels-slider-m-col2 owl-carousel owl-theme">
					<!-- Card -->
					<?php for ($i=1; $i < 6; $i++) { ?>
						<?php include 'inc/cards/card.php' ?>
					<?php } ?>
				</div>
			</div>

			<!-- Ads -->
			<div class="col-12">
				<?php include 'inc/ads/anuncio.php' ?>
			</div>
		</div>
	</div>
</section>


<!-- Banner de divulgação -->
<section class="info">
	<div class="wrap">
		<div class="container">
			<div class="row">
				<div class="col-12 wow fadeInDown">
					<h2 class="title">Mande também o seu novel!</h2>
				</div>
				<div class="col-md-7 wow fadeInUp" data-delay=".3s">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates laborum tenetur quod quaerat placeat quasi aperiam velit, distinctio! Minima, maiores?</p>
				</div>
				<div class="col-12 mt-4 wow fadeInUp" data-delay=".4s">
					<a href="novels.php" class="btn btn-three">Saiba como</a>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="upgrades p-default">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="translation list-inline">
					<!-- title -->
					<div><h1 class="title wow flipInX">Últimas atualizações</h1></div>
					<!-- translation -->
					<div>
						<ul class="nav nav-tabs" id="myTabNews" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="translate-tab" data-toggle="tab" href="#translate" role="tab" aria-controls="translate" aria-selected="true">Traduzida</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="novel-cap" data-toggle="tab" href="#tab-original" role="tab" aria-controls="tab-original" aria-selected="false">Original</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="tab-content" id="tabNews">
					<div class="tab-pane fade show active" id="translate" role="tabpanel" aria-labelledby="translate-tab">
						<div class="table-responsive">
							<table class="table table-hover table-striped mb-0">
								<thead>
									<tr>
										<th scope="col"><i class="fas fa-dna"></i> Gênero</th>
										<th scope="col"><i class="fas fa-book"></i> Título</th>
										<th scope="col"><i class="fas fa-book-open"></i> Nº Capítulo</th>
										<th scope="col"><i class="far fa-clock"></i> Postado em</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div class="tag">
												<span>Fantasia</span>
											</div>
										</td>
										<td class="title">
											<a href="indice-novel.php">Lord of all Realms</a>
										</td>
										<td>
											<a href="single.php">Capítulo 151: Obtaining Recognition</a>
										</td>
										<td>20 min atrás</td>
									</tr><!-- End tr -->

									<tr>
										<td>
											<div class="tag">
												<span>Ficção Científica</span>
											</div>
										</td>
										<td class="title">
											<a href="indice-novel.php">Shuras-wrath</a>
										</td>
										<td>
											<a href="single.php">Capítulo 161: Undying Will</a>
										</td>
										<td>2 horas atrás</td>
									</tr><!-- End tr -->

									<tr>
										<td>
											<div class="tag">
												<span>Fantasia</span>
											</div>
										</td>
										<td class="title">
											<a href="indice-novel.php">Destroyer of Ice and Fire</a>
										</td>
										<td>
											<a href="single.php">Capítulo 5: Still hasn’t had enough?</a>
										</td>
										<td>25 de Março, 2019</td>
									</tr><!-- End tr -->
								</tbody>
							</table>
						</div><!-- /.table-responsive -->
					</div>

					<div class="tab-pane fade" id="tab-original" role="tabpanel" aria-labelledby="tap-original">
						<div class="table-responsive">
							<table class="table table-hover table-striped mb-0">
								<thead>
									<tr>
										<th scope="col"><i class="fas fa-dna"></i> Genre</th>
										<th scope="col"><i class="fas fa-book"></i> Title</th>
										<th scope="col"><i class="fas fa-book-open"></i> N° of chapters</th>
										<th scope="col"><i class="far fa-clock"></i> Posted in</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div class="tag">
												<span>Fantasy</span>
											</div>
										</td>
										<td class="title">
											<a href="indice-novel.php">Lord of all Realms</a>
										</td>
										<td>
											<a href="single.php">Chapter 151: Obtaining Recognition</a>
										</td>
										<td>20 min ago</td>
									</tr><!-- End tr -->

									<tr>
										<td>
											<div class="tag">
												<span>Science fiction</span>
											</div>
										</td>
										<td class="title">
											<a href="indice-novel.php">Shuras-wrath</a>
										</td>
										<td>
											<a href="single.php">Chapter 161: Undying Will</a>
										</td>
										<td>2 hours ago</td>
									</tr><!-- End tr -->

									<tr>
										<td>
											<div class="tag">
												<span>Fantasy</span>
											</div>
										</td>
										<td class="title">
											<a href="indice-novel.php">Destroyer of Ice and Fire</a>
										</td>
										<td>
											<a href="single.php">Chapter 5: Still hasn’t had enough?</a>
										</td>
										<td>March 25, 2019</td>
									</tr><!-- End tr -->
								</tbody>
							</table>
						</div><!-- /.table-responsive -->
					</div>
			</div>
		</div>
	</div>
</section><!-- /.upgrades -->


<section class="p-default pt-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- Tags sugeridas -->
				<h1 class="title wow flipInX">Tags sugeridas</h1>
				<ul class="list-tags">
					<li><a href="#" title="Drama">Drama</a></li>
					<li><a href="#" title="Terror">Terror</a></li>
					<li><a href="#" title="Ficção Científica">Ficção Científica</a></li>
					<li><a href="#" title="Comédia">Comédia</a></li>
					<li><a href="#" title="Suspense">Suspense</a></li>
					<li><a href="#" title="Fantasia">Fantasia</a></li>
					<li><a href="#" title="Academia">Academia</a></li>
					<li><a href="#" title="Jogos">Jogos</a></li>
				</ul>
			</div>
		</div>
	</div>
</section><!-- /.tags -->


<!-- Alerts -->
<section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="alert alert-dark" role="alert">
					<a href="javascript:void(0);">
						<div class="content">
							<h3 class="d-inline-block mb-0">Quer novidades?</h3>
							<p class="d-inline-block mb-0">Veja todas as novels adicionadas recentemente.</p>
						</div><!-- /.content -->
					</a>
				</div><!-- /.alert -->
			</div>
		</div>
	</div>
</section>


<!-- Parceiros -->
<section class="p-default" id="parceiros">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="novels-slider-col2 owl-carousel owl-theme">
					<div><img src="https://via.placeholder.com/265x120" alt=""></div>
					<div><img src="https://via.placeholder.com/265x120" alt=""></div>
					<div><img src="https://via.placeholder.com/265x120" alt=""></div>
					<div><img src="https://via.placeholder.com/265x120" alt=""></div>
					<div><img src="https://via.placeholder.com/265x120" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php include_once 'inc/footer.php'; ?>
