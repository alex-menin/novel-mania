<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#e0e0e0">
	<meta name="description" content="Mergulhe na cultura oriental. Leia Light Novels &amp; WebNovels Chinesas, Japonesas e Coreanas em Português.">

	<title>Início - Novel Mania</title>

	<link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico'/>
	<link rel="stylesheet" href="css/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="fontawesome/css/all.min.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/main.css">

</head>
<body id="scroll-to-top">


<nav class="navbar navbar-expand-lg navbar-light novel-navbar">
	<div class="container">
		<a class="navbar-brand" href="./" title="Novel Mania">
			<img src="img/logo.png" alt="Novel Mania" height="50">
		</a>

		<div class="navbar-right-top ml-auto order-1 order-lg-2">
			<div class="link-nav">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navDrop" aria-controls="navDrop" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>

			<div class="link-nav">
				<ul class="navbar-nav">
		            <li class="nav-item dropdown nav-user">
		                <a class="nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                	<img src="img/user-profile.png" alt="" class="user-avatar-md rounded-circle">
		                </a>
		                <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
		                    <div class="nav-user-info">
		                        <h5 class="mb-0 text-white nav-user-name">Henrique Couto</h5>
		                    </div>
		                    <a class="dropdown-item" href="template-profile.php"><i class="fas fa-user mr-2"></i>Conta</a>
		                    <a class="dropdown-item" href="template-profile.php"><i class="fas fa-cog mr-2"></i>Configurações</a>
		                     <a class="dropdown-item" href="biblioteca.php"><i class="fas fa-book mr-2"></i>Biblioteca</a>
		                    <span class="divider"></span>
		                    <a class="dropdown-item" href="template-profile.php"><i class="fas fa-power-off mr-2"></i>Entrar</a>
		                    <a class="dropdown-item" href="login.php"><i class="fas fa-sign-out-alt mr-2"></i>Sair</a>
		                </div>
		            </li>
		        </ul>
	        </div>
        </div>

		<div class="collapse navbar-collapse order-2 order-lg-1" id="navDrop">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="./"><i class="fa fa-home"></i> Início</a>
				</li>
				<li class="nav-item dropdown-novel">
					<a class="nav-link" href="javascript:void(0);"><i class="fas fa-book-open"></i> Novels</a>
					<ul class="menu">
						<li class="nav-item">
							<a href="#">Todas categorias</a>
						</li>
						<li class="nav-item">
							<a href="#">Categoria 2</a>
						</li>
						<li class="nav-item">
							<a href="#">Categoria 3</a>
						</li>
						<li class="nav-item">
							<a href="#">Categoria 4</a>
						</li>
						<li class="nav-item">
							<a href="#">Categoria 5</a>
						</li>
						<li class="nav-item">
							<a href="#">Categoria 6</a>
						</li>
						<li class="nav-item">
							<a href="#">Categoria 3</a>
						</li>
						<li class="nav-item">
							<a href="#">Categoria 4</a>
						</li>
					</ul>
				</li>
				<li class="nav-item">
					<a class="nav-link search-click" href="#"><i class="fa fa-search"></i> Buscar</a>
					<div class="search-open">
						<span class="close mb-4" title="Fechar"><i class="fas fa-times"></i></span>
						<form action="pesquisa.php" method="post" class="search-form" autocomplete="off">
							<label for="search" class="h2">O que está procurando?</label>
						    <div class="input-group mb-2">
						        <input type="search" class="form-control" name="search" placeholder="Procurar novel...">
						        <div class="input-group-prepend">
									<button type="submit"><i class="fa fa-search"></i></button>
						        </div>
						    </div>
						</form>
					</div><!-- /search-open -->
				</li>
			</ul>
		</div><!-- /.collapse -->
	</div><!-- /.container -->
</nav><!-- /.navabr -->
