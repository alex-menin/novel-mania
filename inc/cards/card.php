<div class="card border mb-4">
	<a href="indice-novel.php" class="link">
        <img src="img/card/img-<?=$i?>.jpg" alt="" class="card-image">
        <div class="card-info">
            <h2 class="card-title">Lorem ipsum dolor sit amet.</h2>
            <div class="evaluation">
                <ul class="list-inline">
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li>4.5</li>
                </ul>
            </div><!-- /.evaluation -->
            <div class="views">
                <i class="fa fa-eye"></i> 2.5k
            </div>
        </div><!-- /.card-info -->
    </a>
</div><!-- /.card -->

