<div class="col-6 col-md-4 col-lg-3">
	<div class="top-novels border-none dark ">
		<div class="row">
			<div class="col-sm-12 mb-0 mb-sm-2">
				<div class="card c-size-2 border">
					<img src="img/card/moana.jpg" alt="" class="card-image">
				</div>
			</div><!-- /.card -->

			<div class="col-sm-12 mt-2 mt-sm-0 mb-3">
				<h2 class="h6 mb-1">Lorem ipsum dolor sit.Lorem ipsum dolor sit amet, consectetur.</h2>
				<div class="author">Autores: Mariana Clara, Luiz de Assis</div>
				<div class="evaluation">
		        	<ul class="list-inline">
		        		<li><i class="fas fa-star"></i></li>
		        		<li><i class="fas fa-star"></i></li>
		        		<li><i class="fas fa-star"></i></li>
		        		<li><i class="fas fa-star"></i></li>
		        		<li><i class="fas fa-star"></i></li>
		        		<li>5.0</li>
		        	</ul>
		        </div><!-- /.evaluation -->
		        <div class="views">
		        	<i class="fa fa-eye"></i> 2.5k
		        </div>
		        <div>
		        	<a href="indice-novel.php" class="btn btn-primary btn-small">Ler novel</a>
		        </div>
			</div>
		</div>
	</div>
</div><!-- /.top-novels -->
