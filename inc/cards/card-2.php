<div class="top-novels dark col-6">
	<div class="row mb-2">
		<div class="col-sm-12 col-lg-4 col-xl-3">
			<div class="card c-size-1 border">
				<img src="http://via.placeholder.com/530x700" alt="" class="card-image">
			</div>
		</div><!-- /.card -->

		<div class="col-sm-12 col-lg-8 col-xl-9 mt-2 mt-sm-0">
			<h5 class="mb-1 mt-sm-2 mt-lg-0">Lorem ipsum dolor sit.Lorem ipsum dolor sit amet, consectetur.</h5>
			<div class="author">Autores: Mariana Clara, Luiz de Assis</div>
			<div class="evaluation">
	        	<ul class="list-inline">
	        		<li><i class="fas fa-star"></i></li>
	        		<li><i class="fas fa-star"></i></li>
	        		<li><i class="fas fa-star"></i></li>
	        		<li><i class="fas fa-star"></i></li>
	        		<li><i class="far fa-star"></i></li>
	        		<li>4.0</li>
	        	</ul>
	        </div><!-- /.evaluation -->
	        <div class="views">
	        	<i class="fa fa-eye"></i> 2.5k
	        </div>
	        <div class="description">
	        	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam dolorum expedita pariatur placeat asperiores, perferendis laborum provident, saepe repudiandae eveniet reiciendis dignissimos quam.</p>
	        </div>
	        <div>
	        	<a href="indice-novel.php" class="btn btn-orange btn-small mb-2 mb-xl-0"><i class="fas fa-chevron-right mr-1"></i> Ler novel</a>
	        	<a href="#" class="btn btn-orange btn-small mb-md-2 mb-lg-0"><i class="fas fa-plus mr-1"></i> Add novel</a>
	        </div>
		</div>
	</div>
</div><!-- /.top-novels -->
