<div class="progress-indicator-2"></div>

<nav class="settings">
	<div class="wrap">
		<ul>
			<li title="Quantidade lida">
				<div class="item">
					<div class="progress-count">0%</div>
					<span class="reading">lido</span>
				</div>
			</li>
			<li>
				<a href="javascript:void(0);">
					<span class="item">
						<i class="far fa-moon"></i>
					</span>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);">
					<span class="item">
						<i class="fas fa-font"></i>
						<span>+</span>
					</span>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);">
					<span class="item">
						<i class="fas fa-font"></i>
						<span>-</span>
					</span>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);">
					<span class="item">
						<i class="fas fa-bold"></i>
					</span>
				</a>
			</li>
			<li>
				<a href="javascript:void(0);">
					<span class="item">
						<i class="fas fa-bars"></i>
					</span>
				</a>
			</li>
		</ul>
	</div>
</nav>
