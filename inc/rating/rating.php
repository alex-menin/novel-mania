<!-- modal evaluation -->
<div class="modal evaluation-option fade" tabindex="-1" role="dialog" aria-labelledby="evaluation" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="evaluation">Avalie</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="dark p-relative">
			<div class="evaluation lange w-450">
				<div>
					<div class="evaluation-individual">
						<span>Qualidade de Tradução</span>
						<div class="right">
							<div class="vote" id="stars-1">
								<ul class="list-inline d-inline-block">
				                    <li class="star" title="Poor" data-value="1">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Fair" data-value="2">
								    	<i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Good" data-value="3">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Excellent" data-value="4">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="WOW!!" data-value="5">
								        <i class="fas fa-star"></i>
								    </li>
				                    <li class="result"><small>0</small></li>
				                </ul><br>
			              	</div>
						</div>
		            </div>
					<div class="evaluation-individual">
						<span>Estabilidade de Atualizações</span>
						<div class="right">
							<div class="vote" id="stars-2">
								<ul class="list-inline d-inline-block">
				                    <li class="star" title="Poor" data-value="1">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Fair" data-value="2">
								    	<i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Good" data-value="3">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Excellent" data-value="4">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="WOW!!" data-value="5">
								        <i class="fas fa-star"></i>
								    </li>
				                    <li class="result"><small>0</small></li>
				                </ul><br>
			              	</div>
						</div>
		            </div>
					<div class="evaluation-individual">
						<span>Desenvolvimento de História</span>
						<div class="right">
							<div class="vote" id="stars-3">
								<ul class="list-inline d-inline-block">
				                    <li class="star" title="Poor" data-value="1">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Fair" data-value="2">
								    	<i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Good" data-value="3">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Excellent" data-value="4">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="WOW!!" data-value="5">
								        <i class="fas fa-star"></i>
								    </li>
				                    <li class="result"><small>0</small></li>
				                </ul><br>
			              	</div>
						</div>
		            </div>
					<div class="evaluation-individual">
						<span>Design de personagem</span>
						<div class="right">
							<div class="vote" id="stars-4">
								<ul class="list-inline d-inline-block">
				                    <li class="star" title="Poor" data-value="1">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Fair" data-value="2">
								    	<i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Good" data-value="3">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Excellent" data-value="4">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="WOW!!" data-value="5">
								        <i class="fas fa-star"></i>
								    </li>
				                    <li class="result"><small>0</small></li>
				                </ul><br>
			              	</div>
						</div>
		            </div>
					<div class="evaluation-individual">
						<span>Fundo Mundial</span>
						<div class="right">
							<div class="vote" id="stars-5">
								<ul class="list-inline d-inline-block">
				                    <li class="star" title="Poor" data-value="1">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Fair" data-value="2">
								    	<i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Good" data-value="3">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="Excellent" data-value="4">
								        <i class="fas fa-star"></i>
								    </li>
								    <li class="star" title="WOW!!" data-value="5">
								        <i class="fas fa-star"></i>
								    </li>
				                    <li class="result"><small>0</small></li>
				                </ul><br>
			              	</div>
						</div>
		            </div>
				</div><!-- /.evaluation- individual -->
			</div><!-- /.evaluation -->
			<div class="mb-2 w-12 text-grey">
	      		<small>Obs: Clique nas estrelinhas de 1 a 5 para avaliar</small>
	      	</div>
	      	<div class="success-box">
			    <img alt="Avaliado!" width="32" src="img/content/verified.svg"/>
			    <div class="text-message"></div>
		    </div>
		</div><!-- /.dark -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Enviar avaliação</button>
      </div>
    </div>
  </div>
</div>
