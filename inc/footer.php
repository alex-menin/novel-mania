<footer class="novel-footer">
	<div class="wrap-info">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-3 text-center text-md-left mb-4 mb-lg-0">
					<div class="logo">
						<a href="./">
							<img src="img/logo.png" alt="Novel Mania">
						</a>
						<span class="text-center">Novel Mania</span>
						<!-- Social -->
						<ul class="social list-inline mt-3">
							<li><a href="https://facebook.com/novelmania" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="https://twitter.com/novelmaniabr" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-twitter"></i></a></li>
							<li><a href="https://www.instagram.com/novelmaniaoficial/" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-instagram"></i></a></li>
							<li><a href="https://youtube.com/c/NovelMania" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-youtube"></i></a></li>
							<li class="mt-2"><a href="https://discordapp.com/invite/NxBMDMz" target="_blank" rel="noopener" aria-label="Rede Social"><i class="fab fa-discord"></i></a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6 col-lg-3 text-center text-md-left mb-4 mb-lg-0">
					<h1>Mapa do site</h1>
					<ul class="list-inline mb-0">
						<li><a href="./">Início</a></li>
						<li><a href="template-blog.php">Blog</a></li>
						<li><a href="template-sobre.php">Sobre</a></li>
						<li><a href="template-editoria.php">Editoria</a></li>
						<li><a href="template-salao-de-contribuicao.php">Doações</a></li>
						<li><a href="template-regras-setoriais.php">Regras Setoriais</a></li>
						<li><a href="template-politica-de-privacidade.php">Política de Privacidade</a></li>
						<li><a href="template-contato.php">Contato</a></li>
					</ul>
				</div>

				<div class="col-md-6 col-lg-3 text-center text-md-left mb-4 mb-md-0">
					<h1>WebNovel</h1>
					<ul class="list-inline mb-0">
						<li><a href="novels.php">Chinesa</a></li>
						<li><a href="novels.php">Japonesa</a></li>
						<li><a href="novels.php">Coreana</a></li>
					</ul>
				</div>

				<div class="col-md-6 col-lg-3 text-center text-md-left">
					<h1>Doações</h1>
					<p class="mb-0">Precisamos da força de todos, uma união para que possamos continuar, <a href="template-salao-de-contribuicao.php">clique aqui</a> para doar qualquer valor.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-6 text-center text-md-left">
					<p>&copy; 2019 Novel Mania, todos os direitos reservados.</p>
				</div>

				<div class="col-md-6 text-center text-md-right mt-2 mt-md-0">
					<p>Desenvolvido por <a href="https://github.com/alexmeninf" target="_blank" rel="noopener">Menin</a></p>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Return top -->
<div class="return-top">
	<a href="#scroll-to-top" aria-label="return top" rel="data-smooth" title="Voltar ao topo">
		<span class="fa fa-arrow-up"></span>
	</a>
</div>

<script type="text/javascript" src="js/vendor/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="plugins/parallax/parallax.js"></script>
<script type="text/javascript" src="js/novel.js"></script><!-- Single.php -->
<script type="text/javascript" src="js/main.js"></script>

<!-- Facebook plugin -->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.2"></script>

</body>
</html>
