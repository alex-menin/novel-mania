<?php include_once 'inc/header.php'; ?>


<section class="landing blog">
	<div class="novel-head pt-3" data-parallax="scroll" data-image-src="img/bg/bg1.jpg" class="img-responsive">
		<div class="container">
			<div class="row justify-content-center">
				<!-- Breadcrumb -->
				<div class="col-md-12">
					<nav aria-label="breadcrumb">
					    <ol class="breadcrumb">
					        <li class="breadcrumb-item"><a href="./"><i class="fa fa-home"></i> Início</a></li>
					        <li class="breadcrumb-item"><a href="template-blog.php">Blog</a></li>
					        <li class="breadcrumb-item active" aria-current="page">Atual</li>
					    </ol>
					</nav>
				</div>

				<!-- Content -->
				<div class="col-md-12 text-center">
					<!-- header info -->
					<div class="novel-info">
						<!-- titulo -->
						<h1 class="font-400 mb-2 wow fadeInUp">Titulo da postangem</h1>

						<p class="wow fadeInUp" data-wow-delay=".3s">
							<span class="info-down">
								<i class="fa fa-clock"></i> 25 de junho de 2019
							</span>
							<span class="info-down ml-sm-2">
								<i class="fas fa-user"></i> Henrique Admin
							</span>
						</p>

						<div class="share wow fadeInUp">
		            		<ul class="list-inline nav justify-content-center">
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fas fa-share-alt"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-facebook-f"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-twitter"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-facebook-messenger"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-pinterest-p"></i></a>
		            			</li>
		            			<li>
		            				<a href="#" target="_blank" rel="noopener" title="Compartilhe"><i class="fab fa-tumblr"></i></a>
		            			</li>
		            		</ul>
		            	</div><!-- /.share -->
					</div>
				</div><!-- /.col-md-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.novel-info -->

	<div class="content-novel p-default dark">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-md-10 col-lg-8">
					<div class="subtitle mb-4 text-center">
						<p>Subtitulo Lorem ipsum dolor sit amet.</p>
					</div><hr>

					<div class="text">
						<p>Cumque odio, nisi rem culpa at maiores? Suscipit aliquid, quibusdam praesentium odit ullam officiis, officia quae! Dicta eius voluptas, optio magni non ex sunt ad molestiae unde impedit repellat odloribus quaerat aliquid officia vel saepe autem earum, modi, aperiam, repellendus quisquam ullam consequuntur ab iusto hic. Dolores non laboriosam accusamus explicabo quam sed provident fuga et unde suscipit numquam, tenetur velit qui iusto ratione adipisci esse dolorum ullam doloribus!</p>
						<div class="text-center">
							<img src="https://via.placeholder.com/500x200" alt="">
						</div>
						<ul class="list1">
							<li>Lorem ipsum dolor sit.</li>
							<li>Lorem ipsum dolor sit.</li>
							<li>Lorem ipsum dolor sit.</li>
							<li>Lorem ipsum dolor sit.</li>
							<li>Lorem ipsum dolor sit.</li>
						</ul>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea itaque, facere dignissimos aliquam eaque velit at iure cupiditate quam ipsam in assumenda unde quae id fuga dolores reprehenderit laborum totam.</p>
					</div><!-- /.text -->

					<hr>

		           	<div class="comments">
		           		<h3 class="mb-0"><i class="far fa-comments"></i> Comentários</h3>
		           		<div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="5"></div>
		           	</div><!-- /.comments -->

				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.content-novel -->
</section>


<?php include_once 'inc/footer.php'; ?>
