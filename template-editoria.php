<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="landing p-default s-border">
	<div class="container">
		<article class="content row">
			<div class="col-12 text-center">
				<img src="img/content/editoria.png" alt="Novel Editoria" width="237" height="87">
				<p class="subtitle">Abaixo estão descritos os dois caminhos para novas histórias na Novel Mania, sejam elas traduções ou histórias originais. Qualquer dúvida, teremos o prazer de sanar.</p>
				<img src="img/content/original.png" alt="Novel Original" width="237" height="87">
				<p class="subtitle mb-4">Abaixo, as vantagens, condições e passos para lançar sua história original na Novel Mania.</p>
			</div>

			<div class="col-lg-6">
				<h3><strong>Vantagens</strong></h3>
				<ul class="list1">
					<li>Todos os meses a Novel Mania tem mais de 3 milhões de visualizações de página, e com isso a sua novel poderá ser lida por milhares de leitores.</li>
					<li>Estratégias definidas para popularizar a história, inclusive acesso direto ao blog Palácio do Dao, para artigos que incentivem a leitura da Novel.</li>
				</ul>
			</div>

			<div class="col-lg-6">
				<h3><strong>Condições</strong></h3>
				<ul class="list1">
					<li>A história precisa ter o roteiro estabelecido – começo, meio e fim, e isso deve estar documentado em um arquivo no Word, para que analisemos a qualidade da narrativa e a proposta da novel.</li>
					<li>A novel não pode ser publicada em nenhum outro site além da Novel Mania.</li>
					<li>A novel não pode ser um plágio de outra história, seja ela estrangeira ou brasileira.</li>
					<li>Um volume/livro da novel deverá já estar pronto para ser iniciado os lançamentos – a quantidade de capítulos por volume é definida pelo autor, não há interferência da Novel Mania.</li>
				</ul>
			</div>

			<div class="col-lg-6">
				<h3><strong>Passos</strong></h3>
				<ul class="list1">
					<li>Envie o roteiro da história descrevendo como você imagina o começo, meio e fim da novel, em um arquivo .doc para o e-mail da Novel Mania.</li>
					<li>Envie os primeiros capítulos da novel em arquivo .doc para o e-mail da Novel Mania, junto ao roteiro.</li>
					<li>No corpo do e-mail descreva quantos capítulos terá o primeiro volume/livro da novel bem como detalhes pertinentes e inspirações para a história.</li>
					<li>Analisaremos a originalidade da obra, qualidade do texto da obra &#8211; se é bem escrita, se é coerente, se o autor tem uma voz própria, se os personagens são bem construídos e responderemos o e-mail em até 7 dias com o parecer final.</li>
				</ul>
			</div>

			<div class="col-lg-12 text-center mt-4 mb-5">
				<hr>
				<img src="https://i0.wp.com/novelmania.com.br/wp-content/uploads/2017/09/Tradução.png?resize=237%2C87" alt="Tradução" width="237" height="87"/>
				<p class="subtitle">Abaixo, as condições que nos baseamos para lançar novas traduções na Novel Mania.</p>
			</div>

			<div class="col-lg-6">
				<h3><strong>Sugestões</strong></h3>
				<p>Você pode sugerir uma novel, que você gostaria que a Novel Mania traduzisse, através do e-mail da Novel Mania, as mais sugeridas com toda certeza "entrarão em nossos olhos".</p>
			</div>

			<div class="col-lg-6">
				<h3><strong>Condições</strong></h3>
				<p>A novel não pode estar sendo traduzida por outro site brasileiro.</p>
				<p>Se a novel se encaixa no catálogo da Novel Mania e no perfil dos leitores – analisamos isso com base em conversas nos grupos de novels no Brasil e também por sugestões enviadas por e-mail.</p>
			</div>

			<div class="col-lg-6">
				<h3><strong>Passos</strong></h3>
				<ul class="list1">
					<li>Análise da originalidade da obra.</li>
					<li>Análise da qualidade do texto da obra – se é bem escrita, se é coerente, se o autor tem uma voz própria, se os personagens são bem construídos.</li>
					<li>Definição da equipe do projeto – Tradutor e Editor.</li>
					<li>Definição da frequência do projeto.</li>
				</ul>
			</div>
		</article>
	</div>
</section>


<?php include_once 'inc/footer.php'; ?>
