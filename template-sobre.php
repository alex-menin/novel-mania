<?php include_once 'inc/header.php'; ?>
<?php include_once 'inc/navbar.php'; ?>


<section class="landing p-default s-border">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<article class="content">
					<div class="text-center">
						<img src="img/content/pagoda-1.png" alt="img">
						<h1 class="wow fadeInUp title">A NovelMania</h1>
					</div>
					<p>Há dois anos atrás, surgiu um cultivador que causou um grande alvoroço no mundo online. Ele chamou a si próprio como MP e tinha forçado as principais Seitas a olharem para ele com respeito. Suas profundas técnicas para decifrar textos antigos e as profundas artes que este ensinava o fez adquirir milhares de seguidores e discípulos.</p>
					<p>Assim, este cultivador fundou a Seita Novel Mania, localizada dentro das fronteiras da Internet Brasileira, no centro do continente Mundial. Hoje mantendo sua posição entre as Quatro Grandes Seitas na Internet Brasileira.</p>
					<div class="alert alert-primary text-center">
						<span>Mergulhe em Aventuras Místicas – as Webnovels!</span>
					</div>

					<div class="text-center mt-4">
						<div class="mt-4">
							<img src="img/content/pagoda-1.png" alt="img">
							<h2 class="wow fadeInUp title">Sala de Contribuição</h2>
						</div>
						<p>Você pode contribuir diretamente com o crescimento da Novel Mania e garantir que ela irá continuar subindo aos céus!</p>
						<div class="alert alert-primary">
							<span>» Para mais informações visite o Salão de Contribuição «</span>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
</section>


<?php include_once 'inc/footer.php'; ?>
